package io.neonlightning.NeoBoop;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPlugin;

public class Boop extends JavaPlugin {

    @Override
    public void onEnable() {
        PluginDescriptionFile pdf = getDescription();
        getLogger().info("NeoBoop " + pdf.getVersion() + " Enabled");
        getLogger().info("NeoBoop Developed By " + pdf.getAuthors());
    }
    @Override
    public void onDisable() {
        getLogger().info("NeoBoop Disabled");
    }

    public boolean onCommand(CommandSender sender, Command command, String label, String[] args){
        Player sendingPlayer = (Player) sender;
        if(command.getName().equalsIgnoreCase("boop")){
            if(sendingPlayer.hasPermission("neoboop.boop") || sendingPlayer.isOp()) {
                if (args.length > 1) {
                    sendingPlayer.sendMessage(ChatColor.RED + "Too Many Arguments");
                } else if (args.length < 1) {
                    sendingPlayer.sendMessage(ChatColor.RED + "Not Enough Arguments");
                } else {
                    Player targetPlayer = Bukkit.getServer().getPlayer(args[0]);
                    if (targetPlayer == null) {
                        sendingPlayer.sendMessage(ChatColor.RED + "Player is offline.");
                    } else {
                        if (targetPlayer == sendingPlayer) {
                            //targetPlayer.sendMessage(ChatColor.YELLOW + sendingPlayer.getDisplayName() + ChatColor.GREEN + " Has Booped You");
                            sendingPlayer.sendMessage(ChatColor.GREEN + "You Have Booped Yourself");
                            sendingPlayer.setHealth(sendingPlayer.getHealth() - 1);
                            sendingPlayer.playSound(sendingPlayer.getLocation(), Sound.ENTITY_GENERIC_HURT, 3.0f, 0.5f);
                            return true;
                        } else {
                            targetPlayer.sendMessage(ChatColor.YELLOW + sendingPlayer.getDisplayName() + ChatColor.GREEN + " Has Booped You");
                            sendingPlayer.sendMessage(ChatColor.GREEN + "You Have Booped " + ChatColor.YELLOW + targetPlayer.getDisplayName());
                            sendingPlayer.playSound(sendingPlayer.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 3.0f, 0.5f);
                            return true;
                        }
                    }
                }
            } else {
                sendingPlayer.sendMessage(ChatColor.RED + "You Don't Have Permission To Use This Command.");
            }
        }
        return false;
    }

}
